# What is this
This is a jQuery plugin for doing asynchronuos data loading with status checks.

# Usage
	myData = {
		url	:	'{$base_url}adm/tracks/json_reprocess/',                       // REQUIRED. URL to submit to
		loadingTitle : '<img src="$ajax_loader" alt="Loading" />',			   // REQUIRED. Element for loading. Can be both an IMG or just a text.
		sendData : 	{ name: 'Dainius', location: "Plugin initializator" },	   // OPTIONAL. Additional data to send
		hoverCursor : "FALSE",												   // OPTIONAL. By default cursor will be changed to hand
		timeoutTime : 1000													   // OPTIONAL. Time to wait before second and next reloads. Default is 2000ms
	};
		
	// Apply it to .reprocess class
	$('.reprocess').jsonClickPoller( myData );

# To Do
Ability to unbind element after it's once "processed".
Ability to "collapse" parent element after deletion.
Ability to automaticali trigger loader (chained trigger() function).
