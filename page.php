<html>
<head>
<meta charset='utf-8'>
<!-- jQuery is 1.8.3 now, but leaving latest for learning -->
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="jqAjaxPoller.js"></script>
</head>
<body>

<script>
  $(document).ready(function () {
	
	// Setup data for the plugin. This is necessary minimum data
	myData = {
		url	:	'json.php',													// URL to do a jQuery
		sendData : 	{ name: 'Dainius', location: "Plugin initializator" },	// Data to send
		loadingTitle : '<img src="ajax-loader.gif" />'							// Element for loading
	};
	
	// Use our new plugin
	$('.activate').jsonClickPoller( myData );
	
	// Destroy the 
	$('.activate').jsonClickPoller('destroy');
	
	// Second usage (the object will actually have data initialized)
	$('.activate').jsonClickPoller( myData );
	
	
	//console.trace()
  });
</script>

  <p id="1" class="activate">ONE</p>

  <p id="2" class="activate">TWO</p>
  <p id="3" class="activate">THREE</p>

</body>
</html>