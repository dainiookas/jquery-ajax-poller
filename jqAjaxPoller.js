/*!
 * jQuery ajax poller
 * dainius.serplis@gmail.com
*/
(function( $ ){

	// Define our methods
	var methods = {
		 init : function( options ) {
		
			// Loop through SELECTOR's elements
		    return this.each(function(){
		   
				// Setup data spacing for the object
				var $this = $(this),
					 data = $this.data('jsonClickPoller');
				 
				 // If the plugin hasn't been initialized yet (first usage)
				 if ( ! data ) {

				   // Extend default settings with the ones passed 
				   var settings = $.extend( {
					   timeoutTime : 	2000,
					   responsible :	'TRUE',		// Causes to check if we actually want to user "reload" option in jquery
					   loadingTitle:	'L',		// This is going to be set as innerHTML
					   hoverCursor :	'TRUE'		// Change cursor to "hand" on mouseover
					}, options);
				   
				   
				   // Utilize jQuery's .data to save settings
				   $(this).data('jsonClickPoller', {
					   target : $this,
					   settings : settings
				   });
				   
					// Debug passed options
					//console.dir( settings );
					var data = $this.data('jsonClickPoller');
					
				 }

				    // Do the actual event binding  
				    $(this).bind('click.jsonClickPoller', function(event) {
						
						// Call our own getState method
						methods.getState.call( this, event.target, 'TRUE', data );
						
					});
					
					// Hand hover if needed
					if(data.settings.hoverCursor){
						$(this).hover(function() {
						 $(this).css('cursor','pointer');
						 }, function() {
						 $(this).css('cursor','auto');
						});
					}

		   });
		 },
		 
		 // Let's do the actual check
		 getState : function( dom_target, firstLaunch, pollerSettings ) { 
			
			// If this is the first launch - do not wait
			if( firstLaunch === 'TRUE'){
				var pollerTimer = 0;
			} else {
				console.log("Reload was requested");
				// Use the timeout time we have in settings
				var pollerTimer = pollerSettings.settings.timeoutTime;
			}
			
			// Make the data we need to send
			var getData = $.extend( {
							id : 	dom_target.id,
							firstLaunch: firstLaunch
							}, pollerSettings.settings.sendData);
							
			
			setTimeout(function(){
				
				$.ajax({
				  url: pollerSettings.settings.url,
				  dataType : "json",
				  cache : false,
				  data: getData,
				  beforeSend: function ( xhr ) {
					dom_target.innerHTML=pollerSettings.settings.loadingTitle;
				  }
				}).done(function ( data ) {
				
					// Check if the state is set to reload and recurse (json.php return has 'reload' set to TRUE)
					if(data.reload) {
						// Call myself once again
						methods.getState.call( this, dom_target, 'FALSE', pollerSettings );
					} else {
						// Set the DOM object if we're done
						dom_target.innerHTML=data.state;
					}
				}).fail(function ( data ) {
					// Do something when the request has failed
				  
				}).always(function ( data ) {
					// Always do this in the end
				});

			}, pollerTimer);
		 
		 },
		 // This will basically unbind event listener from jsonClickPoller namespace
		 destroy : function( ) {
		 
			// Debug
			console.log('Destroy method initialized');
		 
		   return this.each(function(){
			 $(this).unbind('.jsonClickPoller');
		   })
		 }
	}
	
	// Routes between internal methods and also handles initialization
	$.fn.jsonClickPoller = function( method ) {
    
		if ( methods[method] ) {
		  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
		  return methods.init.apply( this, arguments );
		} else {
		  $.error( 'Method ' +  method + ' does not exist on jQuery.jsonClickPoller' );
		}    
  
	};

})( jQuery );